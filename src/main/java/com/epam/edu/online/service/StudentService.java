package com.epam.edu.online.service;

import com.epam.edu.online.model.Student;

public class StudentService {

    public String checkStudentGradePointAverage(Student student) {
        if (student.getGradePointAverage() >= Student.PASSING_SCORE) {
            return "Good student";
        } else {
            return "Bad student";
        }
    }

    public String checkStudentAge(Student student) {
        if (student.getAge() >= Student.AGE_OF_MAJORITY) {
            return "Old enough";
        } else {
            return "Too young";
        }
    }
}
