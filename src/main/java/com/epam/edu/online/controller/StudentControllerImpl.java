package com.epam.edu.online.controller;

import com.epam.edu.online.model.MyCostomeException;
import com.epam.edu.online.model.Student;
import com.epam.edu.online.service.StudentService;

public class StudentControllerImpl implements StudentController {

    Student student;
    StudentService service;

    public StudentControllerImpl() {
    }

    public StudentControllerImpl(Student student) {
        this.student = student;
        this.service = new StudentService();
    }

    @Override
    public void printStudent() throws MyCostomeException {
        if (student == null) {
            throw new MyCostomeException("There is no student");
        }
        System.out.println(this.student.toString());
    }

    @Override
    public String checkAge() {
        return service.checkStudentAge(this.student);
    }

    @Override
    public String checkGradePointAverage() {
        return service.checkStudentGradePointAverage(this.student);
    }

    @Override
    public Student getStudent() {
        return this.student;
    }
}
