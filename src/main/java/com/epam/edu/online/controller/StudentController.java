package com.epam.edu.online.controller;

import com.epam.edu.online.model.Student;

public interface StudentController {

    void printStudent() throws Exception;

    String checkAge();

    String checkGradePointAverage();

    Student getStudent();

}
