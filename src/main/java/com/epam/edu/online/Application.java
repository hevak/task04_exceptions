package com.epam.edu.online;

import com.epam.edu.online.view.StudentView;

public class Application {
    public static void main(String[] args) {
        new StudentView().show();
    }
}
