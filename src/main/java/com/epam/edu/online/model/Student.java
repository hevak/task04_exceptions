package com.epam.edu.online.model;

import java.util.Objects;
import java.util.Random;

public class Student {

    public static final double PASSING_SCORE = 8.0;
    public static final int AGE_OF_MAJORITY = 18;
    private String firstName;
    private String secondName;
    private int age;
    private double gradePointAverage;

    public Student(String firstName, String secondName, int age, double gradePointAverage) throws Exception {
        if (age < 16) {
            throw new MyCostomeException("A student cannot be less than 16 years old\n" +
                    "Student was not saved");
        }
        if (gradePointAverage < 1) {
            gradePointAverage = 0;
        }
        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
        this.gradePointAverage = gradePointAverage;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws MyCostomeException{
        if (age < 16) {
            throw new MyCostomeException("A student cannot be less than 16 years old");
        }
        this.age = age;
    }

    public double getGradePointAverage() {
        return gradePointAverage;
    }


    public String getFullName() {
        return this.firstName + " " + this.secondName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return age == student.age &&
                Objects.equals(firstName, student.firstName) &&
                Objects.equals(secondName, student.secondName);
    }


    @Override
    public int hashCode() {
        return Objects.hash(firstName, secondName, age);
    }

    @Override
    public String toString() {
        return "Student{" +
                getFullName()+
                ", age=" + age +
                ", GPA=" + gradePointAverage +
                '}';
    }
}
