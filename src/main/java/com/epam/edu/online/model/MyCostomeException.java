package com.epam.edu.online.model;

public class MyCostomeException extends Exception {
    public MyCostomeException() {
        super();
    }

    public MyCostomeException(String message) {
        super(message);
    }

    public MyCostomeException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyCostomeException(Throwable cause) {
        super(cause);
    }
}
