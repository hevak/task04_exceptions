package com.epam.edu.online.view;

import com.epam.edu.online.controller.StudentControllerImpl;
import com.epam.edu.online.model.MyCostomeException;
import com.epam.edu.online.model.Student;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;

public class StudentView {

    StudentControllerImpl controller = new StudentControllerImpl();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public StudentView() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - new student");
        menu.put("2", "  2 - show current student");
        menu.put("3", "  3 - edit age current student");
        menu.put("4", "  4 - is good student");
        menu.put("5", "  5 - is young student");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {
        Student student = null;
        try {
            student = fillStudent();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        this.controller = new StudentControllerImpl(student);
    }

    private Student fillStudent() throws Exception {
        System.out.print("Enter first name: ");
        String firstName = input.next().trim();
        System.out.print("Enter second name: ");
        String secondName = input.next().trim();
        System.out.print("Enter age: ");
        int age = input.nextInt();
        System.out.print("Enter grade point average: ");
        double gradePointAverage = 0;
        try {
            gradePointAverage = input.nextDouble();
        } catch (NumberFormatException e) {
            System.err.println(e.getMessage());
        }
        return new Student(firstName, secondName, age, gradePointAverage);

    }

    private void pressButton2() {
        try {
            controller.printStudent();
        } catch (MyCostomeException e) {
            System.err.println(e.getMessage());
        }
    }

    private void pressButton3() {
        System.out.print("Enter new age: ");
        try {
            int age = Integer.parseInt(input.next());
            controller.getStudent().setAge(age);
        } catch (NumberFormatException e) {
            System.out.println("A number is needed here");
            pressButton3();
        } catch (MyCostomeException e) {
            System.out.println(e.getMessage());
            pressButton3();
        }
    }

    private void pressButton4() {
        System.out.println(controller.checkAge());
    }

    private void pressButton5() {
        System.out.println(controller.checkGradePointAverage());
    }
    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase().trim();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                System.out.println("Command not found");
            }
        } while (!keyMenu.equals("Q"));
    }

}
